package Kafka.kafkaproducer.controller;

import Kafka.kafkaproducer.model.Student;
import Kafka.kafkaproducer.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.VoidDeserializer;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/publish")
@RequiredArgsConstructor
public class ProducerController {

    private final KafkaTemplate<String,Object> kafkaTemplate;

    @PostMapping("/create")
    public ResponseEntity<Void> publish(@RequestBody Student student){
        List<Header> headers=new ArrayList<>();
        headers.add(new RecordHeader("Accept-Language","az".getBytes()));
        kafkaTemplate.send(new ProducerRecord<>("user-register",null,"1",student,headers));
        return ResponseEntity.ok().build();
    }


    //OBJECT GONDERENDE
//    private final KafkaTemplate<String,Object> kafkaTemplate;
//
//
//    private final StudentRepository studentRepository;
//    @PostMapping("/create")
//    public ResponseEntity<Void> publisStudent(@RequestBody Student student){
//
//        kafkaTemplate.send("Student-topic",null,student);
//        return ResponseEntity.ok().build();
//    }




//STRING GONDERENDE

//    private final KafkaTemplate<String,String> kafkaTemplate;
//    @PostMapping("/{message}")
//    public ResponseEntity<Void> publish(@PathVariable String message){
//        kafkaTemplate.send("smsEventTopic","3",message);
//        return ResponseEntity.ok().build();
//    }
}
